function taxCalc() {
    var income = document.querySelector("#income").value;
    var ms = document.querySelector("#ms").value;
    var yearlyIncome, monthlyTax, yearlyTax, yearlyCashInHand, base;

    yearlyIncome = income * 12;

    if (ms == "M") {
        //console.log(yearlyIncome);
        base = 450000;

        if (yearlyIncome <= base ) {
            yearlyTax = yearlyIncome * 0.01;
        } else if (yearlyIncome > base && yearlyIncome <= (base + 100000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.1);
        } else if (yearlyIncome > (base + 100000) && yearlyIncome <= (base + 100000 + 200000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.2);
        } else if (yearlyIncome > (base + 100000 + 200000) && yearlyIncome <= (base + 100000 + 200000 + 1250000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.3);
        } else if (yearlyIncome > (base + 100000 + 200000 + 1250000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.36);
        }
    } else {
        //console.log(yearlyIncome);
        base = 400000;

        if (yearlyIncome <= base ) {
            yearlyTax = yearlyIncome * 0.01;
        } else if (yearlyIncome > base && yearlyIncome <= (base + 100000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.1);
        } else if (yearlyIncome > (base + 100000) && yearlyIncome <= (base + 100000 + 200000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.2);
        } else if (yearlyIncome > (base + 100000 + 200000) && yearlyIncome <= (base + 100000 + 200000 + 1300000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.3);
        } else if (yearlyIncome > (base + 100000 + 200000 + 1300000)) {
            yearlyTax = (base * 0.01) + ((yearlyIncome-base) * 0.36);
        }
    }

    //console.log(yearlyTax);

    monthlyTax = yearlyTax/12;
    yearlyCashInHand = yearlyIncome - yearlyTax;

    document.querySelector("#yearlyIncome").innerHTML = yearlyCashInHand.toFixed(2);
    document.querySelector("#yearlyTax").innerHTML = yearlyTax.toFixed(2);
    document.querySelector("#monthlyTax").innerHTML = monthlyTax.toFixed(2);
    document.querySelector("#monthlyCash").innerHTML = (income - monthlyTax).toFixed(2);
    
}